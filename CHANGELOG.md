# Changelog

## Release 0.1.0

Hooray, the first release!

**Features**

  - Managing users through RBAC
  - Managing ssh access through RBAC

**Known Issues**

  - Homedirectories other than /home/username are not supported
  - Group membership is not particularly flexible for multiple roles
