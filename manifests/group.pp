# @summary
#   Grants membership of a posix group based on roles.
#
# @example
#   rbac::group { 'foo':
#     group => 'foo',
#     role  => 'myrole'
#   }
#
# @param group
#   The group users with the specified role will be member of.
#
# @param role
#   The role users must have to be member of the given group.
#
# @param additional_members
#   Additional users that should be member of the given group, but do not have
#   the specified role.
#
# Calling rbac::group will ensure that every user with the given role, as well
# as any user in the additional_members array, and only these users, will be
# members of the given group.
#
define rbac::group (
  String           $group,
  Optional[String] $role               = undef,
  Array[String]    $additional_members = [],
) {
  $roles = $rbac::roles

  if $role {
    $members = $roles[$role] + $additional_members
  } else {
    $members = $additional_members
  }

  unless defined(Groupmembership[$group]) {
    @groupmembership { $group:
      members   => [],
      exclusive => true,
      tag       => rbac,
    }
  }

  Groupmembership <| title == $group and tag == rbac |> {
    members +> $members,
  }
}
