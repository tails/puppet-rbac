# @summary
#   Grants ssh access based on roles.
#
# @example
#   rbac::ssh { 'foo': 
#     user => 'bar',
#     role => 'myrole'
#   }
#
# @param user
#   The user to which ssh access will be granted.
#
# @param role
#   The role users must have to have ssh access to the given user.
#
# @param options
#   An array with key options; see sshd(8) for possible values
#
# Calling rbac::ssh will ensure that the ssh keys of everyone in the
# rbac::users hash with the appropriate role will be part of to the 
# authorized_keys for the given user.
#
define rbac::ssh (
  String $user,
  String $role,
  Array  $options = [],
) {
  $roles = $rbac::roles

  $roles[$role].each | String $username | {
    rbac::sshto { "${role}-${username}-${user}":
      grantee => $username,
      granted => $user,
      options => $options,
    }
  }
}
