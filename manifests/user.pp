# @summary
#   Creates users based on their role
#
# @example
#   rbac::user { 'myrole': }
#
# @param role
#   The role users must have to be present and have access to this system.
#
# @param ssh
#   Whether or not ssh access should be managed by rbac
#
# @param mailalias
#   Whether or not mailaliases should be managed by rbac
#
# Calling rbac::user will ensure that every user in the rbac::users hash
# with the given role will be present on the system and accessible using
# the provided ssh keys.
#
define rbac::user (
  String $role       = $title,
  Boolean $ssh       = true,
  Boolean $mailalias = true,
) {
  $users = $rbac::users
  $roles = $rbac::roles

  $roles[$role].each | String $username | {
    User <| title == $username and tag == rbac |> {
      ensure         => present,
      home           => "/home/${username}",
      managehome     => true,
      purge_ssh_keys => $ssh,
      *              => $users[$username]['userparams'],
    }

    if $ssh {
      rbac::sshto { "${role}-${username}-${username}":
        grantee => $username,
        granted => $username,
      }
    }

    if $mailalias and 'email' in $users[$username] {
      ensure_resource('mailalias', $username, {
          ensure    => present,
          recipient => $users[$username]['email'],
      })
    }
  }
}
