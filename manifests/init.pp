# @summary
#   A simple RBAC implementation for managing users
#
# @example
#   include rbac
#
# @param users
#   A hash with user data
#
# @param roles
#   A hash with roles and lists of users having said role
#
# This class ensures that any user in the provided userlist who has
# not been granted access on the system through one of this module's
# defined types is purged from the system.
#
class rbac (
  Hash $users,
  Hash $roles,
) {
  $users.each | String $user, Hash $userdata | {
    @user { $user:
      ensure     => absent,
      home       => "/home/${user}",
      managehome => true,
      tag        => rbac,
    }
  }
  User <| tag == rbac |>
}
