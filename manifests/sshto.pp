# @summary
#   Grants ssh access based on roles.
#
# @api private
#
# @param granted
#   The user to which ssh access will be granted.
#
# @param grantee
#   The user from the rbac::users hash who is to have ssh access.
#
# @param options
#   An array with key options; see sshd(8) for possible values
#
# Calling rbac::sshto will ensure that the ssh keys of the grantee in
# the rbac::users hash will be part of to the authorized_keys for the
# given granted.
#
define rbac::sshto (
  String $granted,
  String $grantee,
  Array  $options = [],
) {
  $users = $rbac::users

  $users[$grantee]['sshkeys'].each | String $keyname, Hash $keydata | {
    if 'ensure' in $keydata {
      $ensure = $keydata['ensure']
    } else {
      $ensure = present
    }
    ensure_resource('ssh_authorized_key',"${grantee}_${keyname}-${granted}", {
        ensure  => $ensure,
        user    => $granted,
        type    => $keydata['type'],
        key     => $keydata['key'],
        options => $options,
      }
    )
  }
}
