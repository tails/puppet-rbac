# rbac

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with rbac](#setup)
    * [What rbac affects](#what-rbac-affects)
    * [Beginning with rbac](#beginning-with-rbac)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)

## Description

This module allows for role based access control of users. It requires having
all your user data in a hash in hiera, along with a hash describing which users
have which role. Access to systems can be granted in your profiles based on the
appropriate role. The rbac module ensures only users with the appropriate role
have access, any other known users are purged.

## Setup

### What rbac affects

This module will purge any user in the users hash that has not explicitly been
granted access.

### Beginning with rbac

You need to have a rbac::users and a rbac::roles entry in your hiera.
For example:

```
rbac::users:
  user1:
    userparams:
      uid: 1001
      shell: /bin/bash
    email: user1@example.org
    sshkeys:
      personal:
        type: ssh-rsa
        key: AAAAACAB...
  user2:
    userparams:
      uid: 1002
      shell: /bin/bash
    email: user2@example.org
    sshkeys:
      personal:
        type: ssh-rsa
        key: AAAA1312...
rbac::roles:
  sysadmins:
    - user1
  webdevs:
    - user1
    - user2
```

## Usage

Start by including rbac:

```
include ::rbac
```

Then, in your profiles, you can set which role is to have access to the system:

```
  rbac::user { 'sysadmins': }
```

This will ensure everyone with the sysadmins role has a user on the system.

You may also want to grant users with a certain role ssh access to a (system)
user:

```
  rbac::ssh { 'webdevs-to-www-data':
    user => 'www-data',
    role => 'webdevs',
  }
```

This will add the ssh keys of everyone with the webdevs role to www-data's
authorized_keys. Note: it is up to you to ensure www-data's authorized_keys are
managed through puppet with purge_ssh_keys set to true, else removing a user 
from the webdevs role may not have the desired result of revoking their access.

Finally, you may grant users membership to a POSIX group based on their role:

```
  rbac::group { 'sysadmins-to-sudo':
    group => 'sudo',
    role  => 'sysadmins',
```

## Limitations

  - Datasources for users and roles other than hiera are not supported.
  - Homedirectories other than /home/username are not supported.
