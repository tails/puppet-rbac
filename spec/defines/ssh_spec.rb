# frozen_string_literal: true

require 'spec_helper'

describe 'rbac::ssh' do
  let(:title) { 'foo' }
  let(:params) do
    {
      user: 'foo',
      role: 'myrole',
    }
  end

  let(:pre_condition) do
    [
      "class { 'rbac':
         users => {
           user1 => {
             userparams => {
               uid     => 1000,
               shell   => '/bin/bash'
             },
             email   => 'user1@example.org',
             sshkeys => {
               personal => {
                 type => 'rsa',
                 key  => 'AAAACAB'
               },
             },
           },
           user2 => {
             userparams => {
               uid     => 1001,
               shell   => '/bin/bash'
             },
             email   => 'user2@example.org',
             sshkeys => {
               personal => {
                 type => 'rsa',
                 key  => 'AAAA312'
               },
             },
           },
         },
         roles => {
           myrole => [ 'user1', 'user2' ],
         },
       }",
    ]
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end
  end
end
